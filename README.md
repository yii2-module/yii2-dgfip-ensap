# yii2-module/yii2-dgfip-ensap

A module to get data from the ensap.gouv.fr website.

![coverage](https://gitlab.com/yii2-module/yii2-dgfip-ensap/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/yii2-module/yii2-dgfip-ensap/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install yii2-module/yii2-dgfip-ensap ^8`


## Configuration

This module needs the following components to be set at the configuration level:

- 'db_dgfip_ensap' should be a \yii\db\Connection


If you already have a database connection, you may use the following trick :

`'db_dgfip_ensap' => function() { return \Yii::$app->get('db'); },`

where 'db' is the id of your database connection.

This module uses the following parameters to be set at the configuration level:

- 'dgfip-ensap-api-username' should be a valid nir number
- 'dgfip-ensap-api-password' should be a valid string corresponding to the nir


Then the module should be configured as follows (in `console.php` or `web.php`) :

```php
$config = [
	...
	'dgfip-ensap' => [
		...
		'currency' => [
			'class' => 'Yii2Module\Yii2DgfipEnsap\DgfipEnsapModule',
		],
		...
	],
	...
];
```


## License

MIT (See [license file](LICENSE))
