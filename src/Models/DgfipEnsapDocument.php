<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-dgfip-ensap library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2DgfipEnsap\Models;

use yii\BaseYii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Connection;

/**
 * This is the model class for table "dgfip_ensap_document".
 * 
 * @property string $dgfip_ensap_document_id The distant uuid of the document
 * @property string $meta_created_at Metadata for the creation of this record
 * @property string $meta_updated_at Metadata for the last update date of this record
 * @property string $dgfip_ensap_document_type_id The id of the related ensap document type
 * @property ?string $user_id The identifier of the user that is connected
 * @property ?string $libelle_1 The first label of the document, if any
 * @property ?string $libelle_2 The second label of the document, if any
 * @property ?string $libelle_3 The third label of the document, if any
 * @property ?string $full_name The full name of the document
 * @property ?string $date_available The date when the document was made available
 * @property ?string $file_path The full path of the file
 * @property ?string $file_name The original name of the file
 * @property ?string $file_mime The original mime type of the file
 * @property ?integer $file_size The original file size
 * @property ?string $file_hash_md5 The md5 of the file
 * @property ?string $file_hash_sha1 The sha1 state of the file
 * @property ?string $file_date_download The last date when this file was downloaded
 * @property ?integer $file_http_status The http status of the file the last time checked
 * 
 * @property DgfipEnsapBulletin $dgfipEnsapBulletin
 * @property DgfipEnsapDocumentType $dgfipEnsapDocumentType
 * 
 * /!\ WARNING /!\
 * This class is generated by the gii module, please do not edit manually this
 * file as the changes may be overritten.
 * 
 * @author Anastaszor
 */
class DgfipEnsapDocument extends ActiveRecord
{
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::tableName()
	 */
	public static function tableName() : string
	{
		return 'dgfip_ensap_document';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::getDb()
	 * @return Connection the database connection used by this AR class
	 * @throws \yii\base\InvalidConfigException
	 * @psalm-suppress MoreSpecificReturnType
	 * @psalm-suppress InvalidNullableReturnType
	 */
	public static function getDb() : Connection
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement, NullableReturnStatement */
		return BaseYii::$app->get('db_dgfip_ensap');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::rules()
	 * @return array<integer, array<integer|string, boolean|integer|float|string|array<integer|string, boolean|integer|float|string>>>
	 */
	public function rules() : array
	{
		return [
			[['dgfip_ensap_document_id', 'dgfip_ensap_document_type_id'], 'required'],
			[['meta_created_at', 'meta_updated_at', 'file_date_download'], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
			[['date_available'], 'date', 'format' => 'php:Y-m-d'],
			[['file_size', 'file_http_status'], 'integer'],
			[['dgfip_ensap_document_id', 'user_id'], 'string', 'max' => 36],
			[['dgfip_ensap_document_type_id'], 'string', 'max' => 2],
			[['libelle_1', 'libelle_2', 'libelle_3', 'full_name', 'file_path'], 'string', 'max' => 255],
			[['file_name'], 'string', 'max' => 127],
			[['file_mime'], 'string', 'max' => 64],
			[['file_hash_md5'], 'string', 'max' => 32],
			[['file_hash_sha1'], 'string', 'max' => 40],
		];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::attributeLabels()
	 * @return array<string, string>
	 */
	public function attributeLabels() : array
	{
		return [
			'dgfip_ensap_document_id' => BaseYii::t('DgfipEnsapModule.Models', 'Dgfip Ensap Document ID'),
			'meta_created_at' => BaseYii::t('DgfipEnsapModule.Models', 'Meta Created At'),
			'meta_updated_at' => BaseYii::t('DgfipEnsapModule.Models', 'Meta Updated At'),
			'dgfip_ensap_document_type_id' => BaseYii::t('DgfipEnsapModule.Models', 'Dgfip Ensap Document Type ID'),
			'user_id' => BaseYii::t('DgfipEnsapModule.Models', 'User ID'),
			'libelle_1' => BaseYii::t('DgfipEnsapModule.Models', 'Libelle 1'),
			'libelle_2' => BaseYii::t('DgfipEnsapModule.Models', 'Libelle 2'),
			'libelle_3' => BaseYii::t('DgfipEnsapModule.Models', 'Libelle 3'),
			'full_name' => BaseYii::t('DgfipEnsapModule.Models', 'Full Name'),
			'date_available' => BaseYii::t('DgfipEnsapModule.Models', 'Date Available'),
			'file_path' => BaseYii::t('DgfipEnsapModule.Models', 'File Path'),
			'file_name' => BaseYii::t('DgfipEnsapModule.Models', 'File Name'),
			'file_mime' => BaseYii::t('DgfipEnsapModule.Models', 'File Mime'),
			'file_size' => BaseYii::t('DgfipEnsapModule.Models', 'File Size'),
			'file_hash_md5' => BaseYii::t('DgfipEnsapModule.Models', 'File Hash Md5'),
			'file_hash_sha1' => BaseYii::t('DgfipEnsapModule.Models', 'File Hash Sha1'),
			'file_date_download' => BaseYii::t('DgfipEnsapModule.Models', 'File Date Download'),
			'file_http_status' => BaseYii::t('DgfipEnsapModule.Models', 'File Http Status'),
		];
	}
	
	/**
	 * @return ActiveQuery
	 * @throws \yii\base\UnknownMethodException
	 */
	public function getDgfipEnsapBulletin() : ActiveQuery
	{
		return $this->hasOne(DgfipEnsapBulletin::class, ['dgfip_ensap_bulletin_id' => 'dgfip_ensap_document_id']);
	}
	
	/**
	 * @return ActiveQuery
	 * @throws \yii\base\UnknownMethodException
	 */
	public function getDgfipEnsapDocumentType() : ActiveQuery
	{
		return $this->hasOne(DgfipEnsapDocumentType::class, ['dgfip_ensap_document_type_id' => 'dgfip_ensap_document_type_id']);
	}
	
}
