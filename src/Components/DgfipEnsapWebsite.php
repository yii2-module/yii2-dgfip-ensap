<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-dgfip-ensap library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2DgfipEnsap\Components;

use DateTimeImmutable;
use InvalidArgumentException;
use Iterator;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapBulletinLineInterface;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapDocumentInterface;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEndpointInterface;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Parser\ParseThrowable;
use PhpExtended\Uuid\UuidParser;
use PhpExtended\Uuid\UuidParserInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Throwable;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2DgfipEnsap\DgfipEnsapModule;
use Yii2Module\Yii2DgfipEnsap\Models\DgfipEnsapBulletin;
use Yii2Module\Yii2DgfipEnsap\Models\DgfipEnsapDocument;
use Yii2Module\Yii2DgfipEnsap\Models\DgfipEnsapTraitement;

/**
 * DgfipEnsapWebsite class file.
 * 
 * This object is the real crawler of the website. It retrieves resource pages
 * and converts them into module objects, and fills their informations. It
 * follows the workflow used by the update commands, handle html, url and other
 * encodings for all pages of the ensap.gouv.fr website.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class DgfipEnsapWebsite extends ObjectUpdater
{
	
	public const DOCUMENT_TYPE_ATTESTATION_FISCALE = 'AF';
	public const DOCUMENT_TYPE_BULLETIN_PAYE = 'BP';
	public const DOCUMENT_TYPE_DECOMPTE_RAPPEL = 'DR';
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The ensap api.
	 * 
	 * @var ApiFrGouvEnsapEndpointInterface
	 */
	protected ApiFrGouvEnsapEndpointInterface $_endpoint;
	
	/**
	 * The uuid parser.
	 * 
	 * @var ?UuidParserInterface
	 */
	protected ?UuidParserInterface $_uuidParser = null;
	
	/**
	 * Gets whether we are currently logged in.
	 *
	 * @var boolean
	 */
	protected bool $_logged = false;
	
	/**
	 * The login when logged in.
	 * 
	 * @var ?string
	 */
	protected ?string $_login = null;
	
	/**
	 * The years of treatment.
	 * 
	 * @var array<integer, integer>
	 */
	protected array $_years = [];
	
	/**
	 * The folder path where to put files.
	 *
	 * @var string
	 */
	protected string $_folderPath;
	
	/**
	 * Builds a new website with the given api.
	 * 
	 * @param LoggerInterface $logger
	 * @param ApiFrGouvEnsapEndpointInterface $endpoint
	 * @param string $folderPath
	 * @throws InvalidArgumentException
	 */
	public function __construct(LoggerInterface $logger, ApiFrGouvEnsapEndpointInterface $endpoint, string $folderPath)
	{
		$this->_logger = $logger;
		$this->_endpoint = $endpoint;
		$this->_folderPath = $folderPath;
		if(!\is_dir($this->_folderPath) || !\is_writable($this->_folderPath))
		{
			throw new InvalidArgumentException('The directory '.$folderPath.' does not exists or is not writable');
		}
	}
	
	/**
	 * Builds a new website with the given api.
	 * 
	 * @return ApiFrGouvEnsapEndpointInterface
	 */
	public function getEndpoint()
	{
		return $this->_endpoint;
	}
	
	/**
	 * Logs in with the given username and password.
	 * 
	 * @param string $username
	 * @param string $password
	 * @return boolean
	 * @throws RuntimeException if the login fails
	 */
	public function login($username, $password)
	{
		try
		{
			$this->_endpoint->login($username, $password);
		}
		catch(Throwable $e)
		{
			throw new RuntimeException('Failed to log in with given username and password'.$e->getMessage(), -1, $e);
		}
		
		try
		{
			$habilitations = $this->_endpoint->initialiserHabilitations();
			$this->_years = $habilitations->getListeAnneeRemuneration();
		}
		catch(Throwable $e)
		{
			throw new RuntimeException('Failed to initialize habilitations : '.$e->getMessage(), -1, $e);
		}
		
		$this->_login = $username;
		
		return $this->_logged = true;
	}
	
	/**
	 * Gets the list of years.
	 * 
	 * @return array<integer, integer>
	 * @throws RuntimeException if the website is not logged
	 */
	public function getYearIterator() : array
	{
		if(false === $this->_logged)
		{
			throw new RuntimeException('To get the remuneration data, we must first log in.');
		}
		
		return $this->_years;
	}
	
	/**
	 * Gets the remuneration iterator.
	 *
	 * @param integer $year
	 * @return Iterator<integer, ApiFrGouvEnsapDocumentInterface>
	 * @throws ClientExceptionInterface
	 * @throws RuntimeException
	 * @throws UnprovidableThrowable
	 */
	public function getRemunerationIterator(int $year)
	{
		if(false === $this->_logged)
		{
			throw new RuntimeException('To get the remuneration data, we must first log in.');
		}
		
		return $this->_endpoint->getDataRemuneration($year);
	}
	
	/**
	 * Processes the remunerations for the given year.
	 * 
	 * @param integer $year
	 * @return integer the number of records saved
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @throws UnprovidableThrowable
	 */
	public function processRemunerations(int $year) : int
	{
		$count = 0;
		
		/** @var ApiFrGouvEnsapDocumentInterface $apiDocument */
		foreach($this->getRemunerationIterator($year) as $apiDocument)
		{
			$object = $this->saveObjectClass(DgfipEnsapDocument::class, [
				'dgfip_ensap_document_id' => $apiDocument->getDocumentUuid()->getCanonicalRepresentation(),
			], [
				'user_id' => $this->_login,
				'libelle_1' => $apiDocument->getLibelle1(),
				'libelle_2' => $apiDocument->getLibelle2(),
				'libelle_3' => $apiDocument->getLibelle3(),
				'full_name' => \trim((string) \preg_replace('#\\(.*\\)#', '', (string) $apiDocument->getLibelle2())),
				'dgfip_ensap_document_type_id' => $this->getTypeDocumentId($apiDocument->getLibelle2()),
				'date_available' => $apiDocument->getDateDocument()->format('Y-m-d'),
			]);
			
			$count += (int) $object->isNewRecord;
		}
		
		return $count;
	}
	
	/**
	 * Downloads the raw document and updates the correspoding document record.
	 * 
	 * @param DgfipEnsapDocument $document
	 * @return integer the number of records saved
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 * @throws UnprovidableThrowable
	 */
	public function downloadRawDocument(DgfipEnsapDocument $document) : int
	{
		$fichier = $this->_endpoint->getRawDocument($this->getUuidParser()->parse($document->dgfip_ensap_document_id));
		
		$fileFullPath = ((string) $document->user_id)
			.'/'.((string) \mb_substr((string) $fichier->getFileName(), 0, 4))
			.'/'.((string) \str_replace('.pdf', '', (string) $fichier->getFileName()))
			.'_'.((string) $document->dgfip_ensap_document_id).'.pdf';
		
		if($this->saveFile($fileFullPath, $fichier->getRawData()))
		{
			$object = $this->saveObject($document, [], [
				'file_date_download' => (new DateTimeImmutable())->format('Y-m-d H:i:s'),
				'file_http_status' => $fichier->getStatusCode(),
				'file_path' => $fileFullPath,
				'file_name' => $fichier->getFileName(),
				'file_mime' => $fichier->getMimeType(),
				'file_size' => (string) \mb_strlen((string) $fichier->getRawData()),
				'file_hash_md5' => \md5((string) $fichier->getRawData()),
				'file_hash_sha1' => \sha1((string) $fichier->getRawData()),
			]);
			
			return (int) $object->isNewRecord;
		}
		
		return 0;
	}
	
	/**
	 * Processes the given document with bulletin lines.
	 * 
	 * @param DgfipEnsapDocument $document
	 * @param boolean $force whether to redo the already done
	 * @return integer the number of records saved
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function processBulletin(DgfipEnsapDocument $document, bool $force = false) : int
	{
		$bulletin = DgfipEnsapBulletin::findOne($document->dgfip_ensap_document_id);
		if(!$force && null !== $bulletin)
		{
			$this->_logger->info('Skipped to process document '.$document->dgfip_ensap_document_id.' '.((string) $document->full_name).' : bulletin already exists.');
			
			return 0;
		}
		
		$pdfFilePath = DgfipEnsapModule::getInstance()->getAssetsDir().'/'.((string) $document->file_path);
		if(!\is_file($pdfFilePath))
		{
			$this->_logger->error('Skipped to process document '.$document->dgfip_ensap_document_id.' '.((string) $document->full_name).' : file does not exists.');
			
			return 0;
		}
		
		try
		{
			$apiBulletin = $this->_endpoint->getBulletinFromFilePath($pdfFilePath);
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to parse the bulletin for document {uuid} at {path}';
			$context = [
				'{uuid}' => $document->dgfip_ensap_document_id,
				'{path}' => $pdfFilePath,
			];
			
			throw new RuntimeException(\strtr($message, $context), -1, $exc);
		}
		
		$bulletin = $this->saveObjectClass(DgfipEnsapBulletin::class, [
			'dgfip_ensap_bulletin_id' => $document->dgfip_ensap_document_id,
		], [
			'emitter' => (string) $apiBulletin->getEmitter(),
			'emittion_month' => (string) $apiBulletin->getMonthLetter(),
			'num_order' => (string) $apiBulletin->getNumOrder(),
			'work_time' => (string) $apiBulletin->getWorkTime(),
			'poste_1' => (string) $apiBulletin->getPoste1(),
			'poste_2' => (string) $apiBulletin->getPoste2(),
			'libelle_poste' => (string) $apiBulletin->getLibellePoste(),
			'siret_employeur' => (string) $apiBulletin->getSiretEmployeur(),
			'id_min' => (string) $apiBulletin->getIdMin(),
			'id_nir' => \strtr((string) $apiBulletin->getIdNir(), [' ' => '']),
			'id_nodos' => (string) $apiBulletin->getIdNodos(),
			'grade' => (string) $apiBulletin->getGrade(),
			'child_charged' => (string) $apiBulletin->getChildCharged(),
			'echelon' => (string) $apiBulletin->getEchelon(),
			'indice' => (string) $apiBulletin->getIndiceOrNbHours(),
			'hourly_rate' => (string) $apiBulletin->getTauxHoraire(),
			'part_time' => (string) $apiBulletin->getTpsPartiel(),
			'total_a_payer' => (string) $apiBulletin->getTotalAPayer(),
			'total_a_deduire' => (string) $apiBulletin->getTotalADeduire(),
			'total_pour_info' => (string) $apiBulletin->getTotalPourInfo(),
			'total_employeur' => (string) $apiBulletin->getTotalEmployeur(),
			'total_net_a_payer' => (string) $apiBulletin->getTotalNetAPayer(),
			'base_ss_year' => (string) $apiBulletin->getBaseSsYear(),
			'base_ss_month' => (string) $apiBulletin->getBaseSsMonth(),
			'montant_imposable_year' => (string) $apiBulletin->getMontantImposableYear(),
			'montant_imposable_month' => (string) $apiBulletin->getMontahtImposableMonth(),
			'comptable' => (string) $apiBulletin->getComptable(),
			'date_paid' => ($apiBulletin->getDatePaid() ?? new DateTimeImmutable())->format('Y-m-d'),
			'account_paid_iban' => \strtr((string) $apiBulletin->getAccountPaidIban(), [' ' => '']),
			'account_paid_bic' => (string) $apiBulletin->getAccountPaidBic(),
		]);
		
		$count = (int) $bulletin->isNewRecord;
		
		/** @var ApiFrGouvEnsapBulletinLineInterface $apiLine */
		foreach($apiBulletin->getLines() as $k => $apiLine)
		{
			$traitement = $this->saveObjectClass(DgfipEnsapTraitement::class, [
				'dgfip_ensap_traitement_id' => $document->dgfip_ensap_document_id.'_'.\str_pad((string) $k, 2, '0', \STR_PAD_LEFT),
			], [
				'dgfip_ensap_bulletin_id' => $document->dgfip_ensap_document_id,
				'code' => (string) $apiLine->getCode(),
				'libelle' => $apiLine->getLibelle(),
				'a_payer' => (string) $apiLine->getAPayer(),
				'a_deduire' => (string) $apiLine->getADeduire(),
				'pour_info' => (string) $apiLine->getPourInfo(),
			]);
			
			$count += (int) $traitement->isNewRecord;
		}
		
		return $count;
	}
	
	/**
	 * Gets the type of document from the name of the document.
	 * 
	 * @param string $nomDocument
	 * @return string
	 * @throws RuntimeException
	 */
	protected function getTypeDocumentId(?string $nomDocument) : string
	{
		if(null === $nomDocument || '' === $nomDocument)
		{
			throw new RuntimeException('Failed to find type document from empty nom document');
		}
		
		foreach([self::DOCUMENT_TYPE_ATTESTATION_FISCALE, self::DOCUMENT_TYPE_BULLETIN_PAYE, self::DOCUMENT_TYPE_DECOMPTE_RAPPEL] as $type)
		{
			if(\strpos($nomDocument, '_'.$type.'_') !== false)
			{
				return $type;
			}
		}
		
		throw new RuntimeException('Failed to find type document from nom document : '.$nomDocument);
	}
	
	/**
	 * Gets the uuid parser.
	 * 
	 * @return UuidParserInterface
	 */
	protected function getUuidParser() : UuidParserInterface
	{
		if(null === $this->_uuidParser)
		{
			$this->_uuidParser = new UuidParser();
		}
		
		return $this->_uuidParser;
	}
	
	/**
	 * Saves the current file at the given path (makes the directories if
	 * necessary).
	 * 
	 * @param string $filePath
	 * @param string $data
	 * @return int
	 * @throws RuntimeException
	 */
	protected function saveFile(string $filePath, ?string $data) : int
	{
		$rawData = (string) $data;
		$pathChunk = $filePath;
		$fileSlug = \basename($pathChunk);
		$pathChunks = \array_map('trim', \explode('/', $pathChunk));
		$pathChunks = \array_filter($pathChunks, function($value)
		{
			return !(empty($value) || \mb_strpos($value, '.') !== false);
		});
		
		$currentPath = $this->_folderPath;
		
		foreach($pathChunks as $pathChunk)
		{
			$newPath = $currentPath.'/'.$pathChunk;
			if(!\is_dir($newPath))
			{
				if(!\mkdir($newPath, 0755))
				{
					return 0;
				}
			}
			$currentPath = $newPath;
		}
		
		$bytes = \file_put_contents($currentPath.'/'.$fileSlug, $rawData);
		if(false === $bytes)
		{
			throw new RuntimeException('Failed to write data at '.$currentPath.'/'.$fileSlug);
		}
		
		return $bytes;
	}
	
}
