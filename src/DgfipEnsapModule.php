<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-dgfip-ensap library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2DgfipEnsap;

use yii\base\Module;
use yii\BaseYii;
use Yii2Extended\Metadata\Bundle;
use Yii2Extended\Metadata\Record;
use Yii2Module\Helper\BootstrappedModule;
use Yii2Module\Yii2DgfipEnsap\Models\DgfipEnsapBulletin;
use Yii2Module\Yii2DgfipEnsap\Models\DgfipEnsapDocument;
use Yii2Module\Yii2DgfipEnsap\Models\DgfipEnsapDocumentType;
use Yii2Module\Yii2DgfipEnsap\Models\DgfipEnsapTraitement;

/**
 * DgfipEnsapModule class file.
 * 
 * This class is to get and store information from the ensap.gouv.fr module
 * via its website.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class DgfipEnsapModule extends BootstrappedModule
{
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getBootstrapIconName()
	 */
	public function getBootstrapIconName() : string
	{
		return 'file-spreadsheet';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getLabel()
	 */
	public function getLabel() : string
	{
		return BaseYii::t('DgfipEnsapModule.Module', 'Ensap');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getEnabledBundles()
	 */
	public function getEnabledBundles() : array
	{
		return [
			'ensap' => new Bundle(BaseYii::t('DgfipEnsapModule.Module', 'Ensap'), [
				'bulletin' => (new Record(DgfipEnsapBulletin::class, 'bulletin', BaseYii::t('DgfipEnsapModule.Module', 'Bulletin')))->enableFullAccess(),
				'document' => (new Record(DgfipEnsapDocument::class, 'document', BaseYii::t('DgfipEnsapModule.Module', 'Document')))->enableFullAccess(),
				'type' => (new Record(DgfipEnsapDocumentType::class, 'type', BaseYii::t('DgfipEnsapModule.Module', 'Document Type')))->enableFullAccess(),
				'traitement' => (new Record(DgfipEnsapTraitement::class, 'traitement', BaseYii::t('DgfipEnsapModule.Module', 'Traitement')))->enableFullAccess(),
			]),
		];
	}
	
}
