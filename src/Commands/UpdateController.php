<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-dgfip-ensap library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2DgfipEnsap\Commands;

use InvalidArgumentException;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEndpoint;
use PhpExtended\HttpClient\ClientFactory;
use RuntimeException;
use Throwable;
use yii\BaseYii;
use yii\console\ExitCode;
use Yii2Module\Helper\Commands\ExtendedController;
use Yii2Module\Yii2DgfipEnsap\Components\DgfipEnsapWebsite;
use Yii2Module\Yii2DgfipEnsap\DgfipEnsapModule;
use Yii2Module\Yii2DgfipEnsap\Models\DgfipEnsapDocument;

/**
 * UpdateController class file.
 * 
 * This update commands has the actions to update the ensap.data.gouv data
 * in the database from the website.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class UpdateController extends ExtendedController
{
	
	/**
	 * Gets the website component from this module.
	 * 
	 * @param boolean $login whether to log in
	 * @return DgfipEnsapWebsite
	 * @throws InvalidArgumentException
	 * @throws RuntimeException if the params to log in do not exists
	 */
	public function getWebsite(bool $login = true) : DgfipEnsapWebsite
	{
		if(!isset(BaseYii::$app->params['dgfip-ensap-api-username']))
		{
			$message = 'Failed to get param {param}';
			$context = ['{param}' => 'dgfip-ensap-api-username'];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		$username = (string) BaseYii::$app->params['dgfip-ensap-api-username'];
		
		if(!isset(BaseYii::$app->params['dgfip-ensap-api-password']))
		{
			$message = 'Failed to get param {param}';
			$context = ['{param}' => 'dgfip-ensap-api-password'];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		$password = (string) BaseYii::$app->params['dgfip-ensap-api-password'];
		
		$clientFactory = new ClientFactory();
		$clientFactory->setLogger($this->getLogger());
		$clientFactory->getConfiguration()->disablePreferCurl();
		$clientFactory->getConfiguration()->getNativeOptions()->getHttpOptions()->setFollowLocation(false);
		$client = $clientFactory->createClient();
		
		$endpoint = new ApiFrGouvEnsapEndpoint($client);
		$website = new DgfipEnsapWebsite($this->getLogger(), $endpoint, DgfipEnsapModule::getInstance()->getAssetsDir());
		
		if($login)
		{
			try
			{
				$website->login($username, $password);
			}
			catch(Throwable $exc)
			{
				$message = 'Failed to log in with username {username}';
				$context = ['{username}' => $username];
				
				throw new RuntimeException(\strtr($message, $context), -1, $exc);
			}
		}
		
		return $website;
	}
	
	/**
	 * Action all.
	 * 
	 * @return integer the error code, 0 if no error
	 */
	public function actionAll() : int
	{
		$ret = $this->actionRemunerations();
		if(ExitCode::OK !== $ret)
		{
			return $ret;
		}
		
		$ret = $this->actionRawDocuments();
		if(ExitCode::OK !== $ret)
		{
			return $ret;
		}
		
		return $this->actionBulletins();
	}
	
	/**
	 * Remuneration action. This action gathers all the data from the module's
	 * website concerning the remunerations and stores them into the database.
	 * 
	 * @param ?integer $yearMin the year min
	 * @param ?integer $yearMax the year max
	 * @return integer the error code, 0 if no error
	 */
	public function actionRemunerations(?int $yearMin = null, ?int $yearMax = null) : int
	{
		return $this->runCallable(function() use ($yearMin, $yearMax) : int
		{
			$website = $this->getWebsite(true);
			$yearIterator = $website->getYearIterator();
			
			foreach($yearIterator as $year)
			{
				if((null === $yearMin || $yearMin <= $year) && (null === $yearMax || $yearMax >= $year))
				{
					$website->processRemunerations($year);
				}
			}

			return ExitCode::OK;
		});
	}
	
	/**
	 * RawDocument action. This action gathers all the data from the module's
	 * website concerning the documents and stores them into the database.
	 * 
	 * @param ?integer $yearMin the year min
	 * @param ?integer $yearMax the year max
	 * @param boolean $onlyNewRecords
	 * @return integer the error code, 0 if no error
	 */
	public function actionRawDocuments(?int $yearMin = null, ?int $yearMax = null, bool $onlyNewRecords = true) : int
	{
		return $this->runCallable(function() use ($yearMin, $yearMax, $onlyNewRecords) : int
		{
			$query = DgfipEnsapDocument::find();
			if(null !== $yearMin)
			{
			if(!empty($yearMin))
			{
				$query = $query->andWhere('date_available >= :min', ['min' => ((string) $yearMin).'-01-01']);
			}
			}
			if(null !== $yearMax)
			{
				$query = $query->andWhere('date_available < :max', ['max' => ((string) ($yearMax + 1)).'-01-01']);
			}
			if($onlyNewRecords)
			{
				$query->andWhere('file_http_status IS NULL OR file_http_status <> 200');
			}
			
			$count = (int) $query->count();
			if(0 < $count)
			{
				$website = $this->getWebsite(true);
				
				/** @var DgfipEnsapDocument $document */
				foreach($query->each() as $document)
				{
					$website->downloadRawDocument($document);
				}
			}

			return ExitCode::OK;
		});
	}
	
	/**
	 * Bulletin action. This action gathers all the data from the known assets
	 * from the documents that are bulletins, and parses them to fill the
	 * different data.
	 * 
	 * @param ?integer $yearMin the year min
	 * @param ?integer $yearMax the year max
	 * @param boolean $onlyNewRecords
	 * @return integer the error code, 0 if no error
	 */
	public function actionBulletins(?int $yearMin = null, ?int $yearMax = null, bool $onlyNewRecords = true) : int
	{
		return $this->runCallable(function() use ($yearMin, $yearMax, $onlyNewRecords) : int
		{
			$query = DgfipEnsapDocument::find();
			if(null !== $yearMin)
			{
				$query = $query->andWhere('date_available >= :min', ['min' => ((string) $yearMin).'-01-01']);
			}
			if(null !== $yearMax)
			{
				$query = $query->andWhere('date_available < :max', ['max' => ((string) ($yearMax + 1)).'-01-01']);
			}
			$query->joinWith('dgfipEnsapDocumentType AS dt');
			$query->andWhere('dt.dgfip_ensap_document_type_id = :dtp', ['dtp' => 'BP']); // bulletin de paye
			$query->orderBy('date_available ASC');
			
			$count = (int) $query->count();
			if(0 < $count)
			{
				$website = $this->getWebsite(false);
				
				/** @var DgfipEnsapDocument $document */
				foreach($query->each() as $document)
				{
					$this->getLogger()->info('Processing document '.$document->dgfip_ensap_document_id.' '.((string) $document->file_name));
					
					$website->processBulletin($document, !$onlyNewRecords);
				}
			}

			return ExitCode::OK;
		});
	}
	
}
