/**
 * Database data required by DgfipEnsapModule
 * 
 * @author Anastaszor
 * @link https://github.com/yii2-module/yii2-dgfip-ensap
 * @license MIT
 */

INSERT INTO dgfip_ensap_document_type (dgfip_ensap_document_type_id, full_name) VALUES
('AF', 'Attestation Fiscale'),
('BP', 'Bulletin de Paye'),
('DR', 'Décompte de Rappel');
