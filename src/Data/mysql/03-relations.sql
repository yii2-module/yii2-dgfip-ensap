/**
 * Database relations required by DgfipEnsapModule
 * 
 * @author Anastaszor
 * @link https://github.com/yii2-module/yii2-dgfip-ensap
 * @license MIT
 */

ALTER TABLE `dgfip_ensap_bulletin`
ADD CONSTRAINT `fk_dgfip_ensap_bulletin_dgfip_ensap_document`
FOREIGN KEY (`dgfip_ensap_document_id`)
REFERENCES `dgfip_ensap_document`(`dgfip_ensap_document_id`)
ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `dgfip_ensap_traitement`
ADD CONSTRAINT `fk_dgfip_ensap_traitement_dgfip_ensap_bulletin`
FOREIGN KEY (`dgfip_ensap_bulletin_id`)
REFERENCES `dgfip_ensap_bulletin`(`dgfip_ensap_bulletin_id`)
ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `dgfip_ensap_document`
ADD CONSTRAINT `fk_ensap_document_ensap_document_type`
FOREIGN KEY (`dgfip_ensap_document_type_id`)
REFERENCES `dgfip_ensap_document_type`(`dgfip_ensap_document_type_id`)
ON DELETE RESTRICT ON UPDATE CASCADE;
