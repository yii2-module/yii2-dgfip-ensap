/**
 * Database structure required by DgfipEnsapModule
 * 
 * @author Anastaszor
 * @link https://github.com/yii2-module/yii2-dgfip-ensap
 * @license MIT
 */

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `dgfip_ensap_document_type`;
DROP TABLE IF EXISTS `dgfip_ensap_document`;
DROP TABLE IF EXISTS `dgfip_ensap_bulletin`;
DROP TABLE IF EXISTS `dgfip_ensap_traitement`;

SET foreign_key_checks = 1;

CREATE TABLE `dgfip_ensap_document_type`
(
	`dgfip_ensap_document_type_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'The unique id of the document type',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last update date of this record',
	`path` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The slugged name of the document type',
	`full_name` VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci COMMENT 'The full name of the document type',
	UNIQUE KEY `dgfip_ensap_document_type_path` (`path`)
)
ENGINE=INNODB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all documents types from ensap';

CREATE TABLE `dgfip_ensap_document`
(
	`dgfip_ensap_document_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'The unique id of the document',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last update date of this record',
	`path` CHAR(36) NOT NULL COLLATE ascii_bin COMMENT 'The distant uuid of the document',
	`dgfip_ensap_document_type_id` INT(11) DEFAULT NULL COMMENT 'The id of the related document type',
	`user_id` CHAR(36) DEFAULT NULL COLLATE ascii_bin COMMENT 'The identifier of the user that is connected',
	`libelle_1` VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci COMMENT 'The first label of the document, if any',
	`libelle_2` VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci COMMENT 'The second label of the document, if any',
	`libelle_3` VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci COMMENT 'The third label of the document, if any',
	`full_name` VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci COMMENT 'The full name of the document',
	`date_available` DATE DEFAULT NULL COMMENT 'The date when the document was made available',
	`file_path` VARCHAR(255) DEFAULT NULL COLLATE ascii_bin COMMENT 'The full path of the file',
	`file_name` VARCHAR(127) DEFAULT NULL COLLATE utf8_general_ci COMMENT 'The original name of the file',
	`file_mime` VARCHAR(64) DEFAULT NULL COLLATE ascii_bin COMMENT 'The original mime type of the file',
	`file_size` INT(11) DEFAULT NULL COMMENT 'The original file size',
	`file_hash_md5` CHAR(32) DEFAULT NULL COLLATE ascii_bin COMMENT 'The md5 of the file',
	`file_hash_sha1` CHAR(40) DEFAULT NULL COLLATE ascii_bin COMMENT 'The sha1 state of the file',
	`file_date_download` DATETIME DEFAULT NULL COMMENT 'The last date when this file was downloaded',
	`file_http_status` INT(11) DEFAULT NULL COMMENT 'The http status of the file the last time checked',
	UNIQUE KEY `dgfip_ensap_document_path` (`path`),
	UNIQUE KEY `dgfip_ensap_file_path` (`file_path`)
)
ENGINE=INNODB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all documents from ensap';

CREATE TABLE `dgfip_ensap_bulletin`
(
	`dgfip_ensap_bulletin_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'The unique id of the bulletin',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last update date of this record',
	`dgfip_ensap_document_id` INT(11) DEFAULT NULL COMMENT 'The id of the related document',
	`path` CHAR(36) NOT NULL COLLATE ascii_bin COMMENT 'The path of the bulletin (uuid of the document)',
	`emitter` VARCHAR(80) DEFAULT NULL COLLATE utf8_general_ci COMMENT 'The entity that emitted the bulletin',
	`emittion_month` VARCHAR(80) DEFAULT NULL COLLATE utf8_general_ci COMMENT 'The month this bulletin was emitted',
	`num_order` VARCHAR(20) DEFAULT NULL COLLATE ascii_bin COMMENT 'The num order of the bulletin',
	`work_time` VARCHAR(20) DEFAULT NULL COLLATE ascii_bin COMMENT 'The work time for the agent',
	`poste_1` VARCHAR(20) DEFAULT NULL COLLATE ascii_bin COMMENT 'The first id of the poste',
	`poste_2` VARCHAR(10) DEFAULT NULL COLLATE ascii_bin COMMENT 'The second id of the poste',
	`libelle_poste` VARCHAR(80) DEFAULT NULL COLLATE utf8_general_ci COMMENT 'The full libelle of the poste',
	`siret_employeur` CHAR(14) DEFAULT NULL COLLATE ascii_bin COMMENT 'The siret number of the employer',
	`id_min` INT(11) DEFAULT NULL COMMENT 'The min id of the agent',
	`id_nir` CHAR(15) DEFAULT NULL COLLATE ascii_bin COMMENT 'The no_secu of the agent',
	`id_nodos` INT(11) DEFAULT NULL COMMENT 'The no dossier of the agent',
	`grade` VARCHAR(80) DEFAULT NULL COLLATE utf8_general_ci COMMENT 'The full name of the grade of the agent',
	`child_charged` INT(11) DEFAULT NULL DEFAULT 0 COMMENT 'The quantity of children at charge',
	`echelon` INT(11) DEFAULT NULL COMMENT 'The echelon of the agent',
	`indice` INT(11) DEFAULT NULL COMMENT 'The index of the agent',
	`hourly_rate` INT(11) DEFAULT NULL DEFAULT 100 COMMENT 'The hourly rate of the agent',
	`part_time` BOOLEAN DEFAULT FALSE COMMENT 'Whether the agent is at part time',
	`total_a_payer` INT(11) DEFAULT NULL DEFAULT 0 COMMENT 'The brut amount of money to be paid',
	`total_a_deduire` INT(11) DEFAULT NULL DEFAULT 0 COMMENT 'The amount of money to deduce',
	`total_pour_info` INT(11) DEFAULT NULL DEFAULT 0 COMMENT 'The amount of employer taxes',
	`total_employeur` INT(11) DEFAULT NULL DEFAULT 0 COMMENT 'The full amount the employer has to pay',
	`total_net_a_payer` INT(11) DEFAULT NULL DEFAULT 0 COMMENT 'The net amount of money to be paid',
	`base_ss_year` INT(11) DEFAULT NULL DEFAULT 0 COMMENT 'The base ss for the year',
	`base_ss_month` INT(11) DEFAULT NULL DEFAULT 0 COMMENT 'The base ss for the month',
	`montant_imposable_year` INT(11) DEFAULT NULL DEFAULT 0 COMMENT 'The taxable amount of the year',
	`montant_imposable_month` INT(11) DEFAULT NULL DEFAULT 0 COMMENT 'The taxable amount of the month',
	`comptable` VARCHAR(80) DEFAULT NULL COLLATE utf8_general_ci COMMENT 'The comptability office',
	`date_paid` DATE DEFAULT NULL COMMENT 'The date this bulletin was paid',
	`account_paid_iban` CHAR(27) DEFAULT NULL COLLATE ascii_bin COMMENT 'The iban that was used to be paid',
	`account_paid_bic` CHAR(11) DEFAULT NULL COLLATE ascii_bin COMMENT 'The bic that was used to be paid',
	UNIQUE KEY `dgfip_ensap_bulletin_document` (`dgfip_ensap_document_id`)
)
ENGINE=INNODB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all data about bulletins from ensap';

CREATE TABLE `dgfip_ensap_traitement`
(
	`dgfip_ensap_traitement_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'The unique id of the traitement',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last update date of this record',
	`dgfip_ensap_bulletin_id` INT(11) DEFAULT NULL COMMENT 'The id of the related bulletin',
	`path` CHAR(43) NOT NULL COLLATE ascii_bin COMMENT 'The path of the traitement',
	`code` INT(11) DEFAULT NULL COMMENT 'The code of the traitement',
	`libelle` VARCHAR(80) DEFAULT NULL COLLATE ascii_bin COMMENT 'The actual libelle for this traitement',
	`a_payer` INT(11) DEFAULT NULL DEFAULT 0 COMMENT 'The amount to be paid (EUR cts)',
	`a_deduire` INT(11) DEFAULT NULL DEFAULT 0 COMMENT 'The amount to deduce (EUR cts)',
	`pour_info` INT(11) DEFAULT NULL DEFAULT 0 COMMENT 'The taxes for the employer (EUR cts)'
)
ENGINE=INNODB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all traitements about bulletins from ensap';
